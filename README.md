# Authentication

In order to use the real credentials, you should:

- install [direnv](https://direnv.net/) and get it running
- get the official `.envrc`, which is versioned
- put it in a safe place
- `ln -s /safe/place .evnrc`

In order to be granted access to the real credentials, you must:
- hardly beg the sysadmin for permission
- or, alternatively, gift him with a beer or a retrocomputer

